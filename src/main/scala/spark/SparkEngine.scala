package spark

import org.apache.spark.sql._
import org.apache.spark.sql.types.{IntegerType, StructField, StructType}
import org.apache.spark.sql.functions._
import org.apache.spark.ml.feature.VectorAssembler
import traits.Constant
import scala.annotation.tailrec
import scala.collection.mutable.{Map => MutableMap}

case class SparkEngine() extends Constant {

  private val session: SparkSession = SparkSession.builder()
    .appName("Homicide Report")
    .master("local[*]")
    .config("spark.driver.host", "localhost")
    .getOrCreate()

  /**
    * Loads the database CSV file and read it as DataFrame
    *
    * @return DataFrame of the database
    */
  private def loadCSV: DataFrame = {
    session.read.option("header", value=true).csv(CSV_FILE)
  }

  /**
    * Removes non relevant features from the DataFrame and returns it
    *
    * @return The DataFrame without the features to drop
    */
  private def removeNonRelevantFeatures(): DataFrame = {
    println("Removing non-relevant features...")
    @tailrec
    def rec(featuresToDrop: List[String], df: DataFrame): DataFrame = featuresToDrop match {
      case Nil => df
      case _ => rec(featuresToDrop.tail, df.drop(featuresToDrop.head))
    }
    rec(FEATURES_TO_DROP, loadCSV)
  }

  /**
    * Filter records and remove the ones with unknown fields
    *
    * @return The DataFrame with only the solved crimes
    */
  private def removeRecordsWithUnknownFields(): DataFrame = {
    println("Removing records with unsolved crime...")
    removeNonRelevantFeatures().filter(!_.mkString.contains("Unknown"))
  }

  /**
    * Maps the features classes with an index to be used during the Machine Learning process
    *
    * @param df: DataFrame to map to an index
    */
  private def generateClassesIndexIntoMap(df: DataFrame): MutableMap[String, MutableMap[String, Int]] = {
    val classesMap = MutableMap[String, MutableMap[String, Int]]()
    val columns = df.columns
    for (column <- columns) {
      classesMap += column -> MutableMap()
    }
    for (row <- df.collect) {
      for (col <- columns) {
        val value = row.getString(columns.indexOf(col))
        if (!classesMap(col).contains(value)) {
          classesMap(col) += value -> classesMap(col).size
        }
      }
    }
    classesMap
  }

  /**
    * Converts an age range to a simple value.
    * - 0-17  : 0
    * - 18-34 : 1
    * - 35-49 : 2
    * - 50+   : 3
    * Require age >= 0
    *
    * @param age: Int, the age to convert
    * @return the converted value of the age
    */
  private def abstractAge(age: Int): Int = {
    require(age >= 0)
    age match {
      case _ if age < 18 => 0
      case _ if age >= 18 && age < 35 => 1
      case _ if age >= 35 && age < 50 => 2
      case _ => 3
    }
  }

  /**
    * Processes the database file into DataFrame, removing non-relevant features, unsolved crime, etc
    *
    * @return The final preprocessed DataFrame
    */
  def processDatabase(): DataFrame = {
    println("Processing database file...")
    val tempDataFrame = removeRecordsWithUnknownFields()
    val classesMap = generateClassesIndexIntoMap(tempDataFrame)
    val schema = StructType(SCHEMA.map(fieldName => StructField(fieldName, IntegerType, nullable=true)))
    session.sqlContext.createDataFrame(tempDataFrame.rdd.map(row =>
      Row(
        classesMap(CITY_FIELD_NAME)(row.getString(0)),
        classesMap(STATE_FIELD_NAME)(row.getString(1)),
        row.getString(2).toInt,
        classesMap(MONTH_FIELD_NAME)(row.getString(3)),
        classesMap(VICTIM_SEX_FIELD_NAME)(row.getString(4)),
        abstractAge(row.getString(5).toInt),
        classesMap(VICTIM_RACE_FIELD_NAME)(row.getString(6)),
        classesMap(VICTIM_ETHNICITY_FIELD_NAME)(row.getString(7)),
        classesMap(PERPETRATOR_SEX_FIELD_NAME)(row.getString(8)),
        abstractAge(row.getString(9).toInt),
        classesMap(PERPETRATOR_RACE_FIELD_NAME)(row.getString(10)),
        classesMap(PERPETRATOR_ETHNICITY_FIELD_NAME)(row.getString(11)),
        classesMap(RELATIONSHIP_FIELD_NAME)(row.getString(12)),
        classesMap(WEAPON_FIELD_NAME)(row.getString(13))
      )
    ), schema)
  }

  /**
    * Processes the database using the provided label to process the DataFrame into the right format
    *
    * @param label: String, the label which will be used during the Machine Learning process
    * @return The final preprocessed DataFrame ready to be used in the Machine Learning process
    */
  def getDataFrameForMachineLearning(df: DataFrame, label: String): DataFrame = {
    val labels_field_name = "label"
    val features_field_name = "features"
    val toDouble = udf[Double, Int](_.toDouble)
    val assembler = new VectorAssembler().setInputCols(FEATURES).setOutputCol(features_field_name)
    assembler.transform(df)
      .withColumn(labels_field_name, toDouble(df(label)))
      .select(labels_field_name, features_field_name)
  }
}