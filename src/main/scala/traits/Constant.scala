package traits

trait Constant {

  /**
    * Homicide database.csv file
    */
  val ZIP_FILE = "src/main/resources/database.csv.zip"
  val CSV_FILE = "src/main/resources/database.csv"

  /**
    * Homicide database.csv headers (features)
    */
  val RECORD_ID_FIELD_NAME = "Record ID"
  val AGENCY_CODE_FIELD_NAME = "Agency Code"
  val AGENCY_NAME_FIELD_NAME = "Agency Name"
  val AGENCY_TYPE_FIELD_NAME = "Agency Type"
  val CITY_FIELD_NAME = "City"
  val STATE_FIELD_NAME = "State"
  val YEAR_FIELD_NAME = "Year"
  val MONTH_FIELD_NAME = "Month"
  val INCIDENT_FIELD_NAME = "Incident"
  val CRIME_TYPE_FIELD_NAME = "Crime Type"
  val CRIME_SOLVED_FIELD_NAME = "Crime Solved"
  val VICTIM_SEX_FIELD_NAME = "Victim Sex"
  val VICTIM_AGE_FIELD_NAME = "Victim Age"
  val VICTIM_RACE_FIELD_NAME = "Victim Race"
  val VICTIM_ETHNICITY_FIELD_NAME = "Victim Ethnicity"
  val PERPETRATOR_SEX_FIELD_NAME = "Perpetrator Sex"
  val PERPETRATOR_AGE_FIELD_NAME = "Perpetrator Age"
  val PERPETRATOR_RACE_FIELD_NAME = "Perpetrator Race"
  val PERPETRATOR_ETHNICITY_FIELD_NAME = "Perpetrator Ethnicity"
  val RELATIONSHIP_FIELD_NAME = "Relationship"
  val WEAPON_FIELD_NAME = "Weapon"
  val VICTIM_COUNT_FIELD_NAME = "Victim Count"
  val PERPETRATOR_COUNT_FIELD_NAME = "Perpetrator Count"
  val RECORD_SOURCE_FIELD_NAME = "Record Source"

  val FEATURES = Array(CITY_FIELD_NAME,
                      STATE_FIELD_NAME,
                      YEAR_FIELD_NAME,
                      MONTH_FIELD_NAME,
                      VICTIM_SEX_FIELD_NAME,
                      VICTIM_AGE_FIELD_NAME,
                      VICTIM_RACE_FIELD_NAME,
                      VICTIM_ETHNICITY_FIELD_NAME,
                      WEAPON_FIELD_NAME)

  /**
    * Non-relevant Features
    */
  val FEATURES_TO_DROP = List(RECORD_ID_FIELD_NAME,
                              AGENCY_CODE_FIELD_NAME,
                              AGENCY_NAME_FIELD_NAME,
                              AGENCY_TYPE_FIELD_NAME,
                              CRIME_TYPE_FIELD_NAME,
                              CRIME_SOLVED_FIELD_NAME,
                              RECORD_SOURCE_FIELD_NAME,
                              PERPETRATOR_COUNT_FIELD_NAME,
                              INCIDENT_FIELD_NAME, VICTIM_COUNT_FIELD_NAME)

  /**
    * Schema features
    */
  val SCHEMA = List(CITY_FIELD_NAME,
                    STATE_FIELD_NAME,
                    YEAR_FIELD_NAME,
                    MONTH_FIELD_NAME,
                    VICTIM_SEX_FIELD_NAME,
                    VICTIM_AGE_FIELD_NAME,
                    VICTIM_RACE_FIELD_NAME,
                    VICTIM_ETHNICITY_FIELD_NAME,
                    PERPETRATOR_SEX_FIELD_NAME,
                    PERPETRATOR_AGE_FIELD_NAME,
                    PERPETRATOR_RACE_FIELD_NAME,
                    PERPETRATOR_ETHNICITY_FIELD_NAME,
                    RELATIONSHIP_FIELD_NAME,
                    WEAPON_FIELD_NAME)
}