import java.io.{File, FileInputStream, FileOutputStream}
import java.util.zip.ZipInputStream

import spark.{MachineLearning, SparkEngine}
import org.apache.commons.io.FileUtils
import org.apache.log4j.Logger
import org.apache.log4j.Level
import traits.Constant

object Launcher extends Constant {

  /**
    * Silences Spark a bit...
    */
  Logger.getLogger("org").setLevel(Level.ERROR)
  Logger.getLogger("akka").setLevel(Level.ERROR)

  /**
    * Extracts the database as the CSV file is too big for GitHub
    */
  private def unzip(): Unit = {
    println("Extracting file " + ZIP_FILE.split("/").last + "...")
    val fis = new FileInputStream(ZIP_FILE)
    val zis = new ZipInputStream(fis)
    Stream.continually(zis.getNextEntry).takeWhile(_ != null).foreach{ _ =>
      val fos = new FileOutputStream(CSV_FILE)
      val buffer = new Array[Byte](1024)
      Stream.continually(zis.read(buffer)).takeWhile(_ != -1).foreach(fos.write(buffer, 0, _))
    }
  }

  /**
    * Deletes the extracted database at the end of the process
    */
  private def delete(): Unit = {
    println("Deleting database...")
    FileUtils.deleteQuietly(new File(CSV_FILE))
  }

  /**
    * Processes the database according to the defined research
    */
  private def processing(): Unit = {
    val spark = SparkEngine()
    val learn = MachineLearning()
    val df = spark.processDatabase()

    println("Predicting perpetrator gender...")
    learn.applyDecisionTree(spark.getDataFrameForMachineLearning(df, PERPETRATOR_SEX_FIELD_NAME))

    println("Predicting perpetrator age...")
    learn.applyDecisionTree(spark.getDataFrameForMachineLearning(df, PERPETRATOR_AGE_FIELD_NAME))

    println("Predicting perpetrator race...")
    learn.applyDecisionTree(spark.getDataFrameForMachineLearning(df, PERPETRATOR_RACE_FIELD_NAME))

    println("Predicting perpetrator ethnicity...")
    learn.applyDecisionTree(spark.getDataFrameForMachineLearning(df, PERPETRATOR_ETHNICITY_FIELD_NAME))

    println("Predicting perpetrator relationship...")
    learn.applyDecisionTree(spark.getDataFrameForMachineLearning(df, RELATIONSHIP_FIELD_NAME))


  }

  def main(args: Array[String]): Unit = {
    println("Welcome to the Homicide data reports !")
    unzip()
    processing()
    delete()
  }
}