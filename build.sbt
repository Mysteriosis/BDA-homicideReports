name := "BDA-homicideReports"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "junit" % "junit" % "latest.release" % "test",
  "org.scalactic" %% "scalactic" % "latest.release",
  "org.apache.spark" %% "spark-core" % "latest.release",
  "org.apache.spark" %% "spark-sql" % "latest.release",
  "org.apache.spark" %% "spark-mllib" % "latest.release"

)